/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.exception;

import org.slf4j.Logger;

import com.aibton.framework.enums.inter.IEnum;
import com.aibton.framework.util.ExceptionUtils;

/**
 * 请求异常--自定义异常
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:11 huzhihui Exp $$
 */
public class RequestException extends RuntimeException {

    /**
     * 枚举信息
     */
    private IEnum     iEnum;

    /**
     * 错误信息
     */
    private String    exMsg;

    /**
     * 未定义的异常存放地方
     */
    private Exception exception;

    /**
     * 默认构造函数
     */
    public RequestException() {
        super();
    }

    /**
     * 普通带错误信息的异常信息
     * @param exMsg 异常信息
     */
    public RequestException(String exMsg) {
        this.exMsg = exMsg;
    }

    /**
     * 枚举类的异常信息
     * @param iEnum 异常枚举
     */
    public RequestException(IEnum iEnum) {
        this.iEnum = iEnum;
    }

    /**
     * 打印异常具体信息
     * @param logger    LOGGER
     * @param exception exception
     * @param exMsg 异常信息
     */
    public RequestException(Logger logger, Exception exception, String exMsg) {
        logger.error(ExceptionUtils.getExceptionString(exception));
        this.exMsg = exMsg;
    }

    /**
     * 打印异常具体信息
     * @param logger    Logger
     * @param exception Exception
     * @param iEnum 异常枚举信息
     */
    public RequestException(Logger logger, Exception exception, IEnum iEnum) {
        logger.error(ExceptionUtils.getExceptionString(exception));
        this.iEnum = iEnum;
    }

    public IEnum getiEnum() {
        return iEnum;
    }

    public void setiEnum(IEnum iEnum) {
        this.iEnum = iEnum;
    }

    public String getExMsg() {
        return exMsg;
    }

    public void setExMsg(String exMsg) {
        this.exMsg = exMsg;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
