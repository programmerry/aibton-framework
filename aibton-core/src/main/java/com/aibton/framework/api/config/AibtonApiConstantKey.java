/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.config;

/**
 * Api框架常量类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/21 13:18 huzhihui Exp $$
 */
public class AibtonApiConstantKey {

    /**
     * API执行引擎对象为空
     */
    public static final String ENGINE_CONTEXT_NULL = "API执行引擎对象为空";

    /**
     * API执行URL为空
     */
    public static final String API_URL_Null        = "API执行URL为空";

    /**
     * API执行器获取失败
     */
    public static final String API_NULL            = "API执行器获取失败";

    /**
     * 请求参数为空
     */
    public static final String REQUEST_DATA_NULL   = "请求参数为空";

    /**
     * API初始化失败，API注解为空
     */
    public static final String API_INIT_ERROR      = "API初始化失败，API注解为空";
}
