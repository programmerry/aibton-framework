/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.api.utils.AibtonApiEngineUtils;
import com.aibton.framework.data.BaseResponse;
import com.aibton.framework.util.ExceptionUtils;
import com.aibton.framework.util.LoggerUtils;

/**
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/28 15:46 huzhihui Exp $$
 */
public class ResponseDataInitHandel implements IBaseApiHandel {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseDataInitHandel.class);

    @Override
    public void doHandel(EngineContext engineContext) {
        Class c = AibtonApiEngineUtils.getGenericRestrictions(engineContext.getAbstractBaseApi(),
            1);
        try {
            BaseResponse baseResponse = (BaseResponse) c.newInstance();
            engineContext.setBaseResponse(baseResponse);
        } catch (Exception e) {
            LoggerUtils.error(LOGGER, ExceptionUtils.getExceptionString(e));
        }
    }
}
