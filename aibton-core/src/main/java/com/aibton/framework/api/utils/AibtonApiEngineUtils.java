/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.utils;

import java.lang.reflect.ParameterizedType;

import com.aibton.framework.api.AbstractBaseApi;

/**
 * api引擎工具类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 18:30 huzhihui Exp $$
 */
public class AibtonApiEngineUtils {

    /**
     * 获取泛型的接口值
     * @param abstractBaseApi   api类
     * @param typeLocation  取第几个泛型
     * @return  Class
     */
    public static Class getGenericRestrictions(AbstractBaseApi abstractBaseApi, int typeLocation) {
        Class c = (Class) ((ParameterizedType) abstractBaseApi.getClass().getGenericSuperclass())
            .getActualTypeArguments()[typeLocation];
        return c;
    }

}
