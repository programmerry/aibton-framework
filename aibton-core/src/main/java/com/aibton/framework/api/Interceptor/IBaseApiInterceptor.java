package com.aibton.framework.api.Interceptor;

import com.aibton.framework.data.BaseRequest;
import com.aibton.framework.data.BaseResponse;

/**
 * API基础拦截接口
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:50 huzhihui Exp $$
 */
public interface IBaseApiInterceptor<I extends BaseRequest, O extends BaseResponse> {

    /**
     * 执行拦截具体内容
     * @param request   请求参数
     * @param response  返回值参数
     */
    void doIntercetpor(I request, O response);
}
