/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.aibton.framework.config.AibtonConstantKey;

/**
 * spring运行上下文
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 9:36 huzhihui Exp $$
 */
public class RuntimeContextUtils {

    private static final Logger LOOGER = LoggerFactory.getLogger(RuntimeContextUtils.class);

    /**
     * 获取当前登录用户的HttpServletRequest
     * @return  HttpServletRequest
     */
    public static HttpServletRequest getHttpServletRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        return request;
    }

    /**
     * 获取当前登录用户token值
     * 如果为空则直接返回页面提示未登录
     * @return  token
     */
    public static String getToken() {
        HttpServletRequest httpServletRequest = RuntimeContextUtils.getHttpServletRequest();
        String token = httpServletRequest.getHeader(AibtonConstantKey.TOKEN);
        return token;
    }
}
